package nl.hsac.xslttest;

import nl.hsac.test.XsltTestHelper;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;


public class AboutXSLTsTest {
    private static final XsltTestHelper HELPER = new XsltTestHelper(AboutXSLTsTest.class);

    @Before
    public void setUp() throws Exception {
        HELPER.setXsltDirectory(HELPER.getExpectedXmlDirectory());
    }

    @Test
    public void testAboutResponseShortVersion() throws Exception {
        checkAboutResponse("1212.2_0021");
    }

    @Test
    public void testAboutResponseLongVersion() throws Exception {
        // we expect the same output, since only the first characters are returned
        checkAboutResponse("1212.2_0021.28543");
    }

    private void checkAboutResponse(String versionString) throws Exception {
        Map<String,String> parameters = new HashMap<String, String>();
        parameters.put("version", versionString);
        parameters.put("build_date", "2011-11-16");

        HELPER.compareRequestOutputToFile("about.xsl",
                parameters,
                "AboutRequest.xml",
                "AboutResponseOutput.xml");
    }

}
