package nl.hsac.xslttest.directorybased;

import nl.hsac.test.DirectoryBasedXsltTestHelper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.Resource;

/**
 * Tests all XSLTs in a directory, by using inputs and expected values from other directory.
 */
public class DirectoryBasedXsltTest {
    private DirectoryBasedXsltTestHelper helper;

    @Before
    public void setUp() throws Exception {
        helper = new DirectoryBasedXsltTestHelper();
        helper.setBaseDir("src/test/resources/nl/hsac/xslttest/directorybased/");
    }

    @Test
    public void testAll() throws Exception {
        Resource[] xslts = helper.findXslts();
        Assert.assertTrue("No resources found", xslts.length > 0);

        boolean allOk = true;
        for (Resource xslt : xslts) {
            if (!helper.isXsltOk(xslt)) {
                allOk = false;
            }
        }
        Assert.assertTrue(allOk);
    }

}
