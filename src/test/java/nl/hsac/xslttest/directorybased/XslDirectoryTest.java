package nl.hsac.xslttest.directorybased;

import nl.hsac.test.XsltDirectoryContext;
import nl.hsac.test.XsltDirectoryRunner;
import org.junit.runner.RunWith;

@RunWith(XsltDirectoryRunner.class)
@XsltDirectoryContext(baseDirectory = "src/test/resources/nl/hsac/xslttest/directorybased/",
                        createdExpectedIfMissing = true)
public class XslDirectoryTest {
}
