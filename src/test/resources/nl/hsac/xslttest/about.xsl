<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
	<xsl:output method="xml" encoding="utf-8" indent="no" omit-xml-declaration="yes" />

	<xsl:param name="version" />
	<xsl:param name="build_date" />

	<xsl:template match="/">
		<xsl:apply-templates select="AboutRequest" />
	</xsl:template>

	<xsl:template match="AboutRequest">
		<AboutResponse>
			<version>
				<xsl:value-of select="substring($version,1,11)" />
			</version>
			<build_date>
				<xsl:value-of select="$build_date" />
			</build_date>
		</AboutResponse>
	</xsl:template>

</xsl:stylesheet>