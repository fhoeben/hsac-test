<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
	<xsl:output method="xml" encoding="utf-8" indent="no" omit-xml-declaration="yes" />

	<xsl:template match="/">
		<xsl:apply-templates select="InfoRequest" />
	</xsl:template>

	<xsl:template match="InfoRequest">
		<InfoResponse>
			<info>
				<xsl:value-of select="text()" />
			</info>
		</InfoResponse>
	</xsl:template>

</xsl:stylesheet>