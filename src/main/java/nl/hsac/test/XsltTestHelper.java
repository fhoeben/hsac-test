package nl.hsac.test;

import com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl;
import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.Difference;
import org.custommonkey.xmlunit.NodeDetail;
import org.custommonkey.xmlunit.Transform;
import org.custommonkey.xmlunit.XMLUnit;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * Helper class for tests of XSLTs.
 */
public class XsltTestHelper {
    static {
        // we don't check whitespace since it doesn't matter and makes the
        // expected outcome more readable
        XMLUnit.setIgnoreWhitespace(true);
    }

    // lookup map for xsd file to validator
    private static final Map<String, Validator> VALIDATORS = new HashMap<String, Validator>();
    // Transformer factory to be used when determining XML diff
    private static final Class<?> DIFF_TRANS_FACT_CLASS = TransformerFactoryImpl.class;
    // charset used when reading (and writing) files
    public static final Charset FILE_ENCODING = Charset.forName("UTF-8");
    public static final String REPOSITORY_LOCATION = "src/main/resources/";

    private String xsltDirectory = REPOSITORY_LOCATION;
    private String expectedXmlDirectory;

    private String xsdFile = null;

    private boolean writeActual = false;
    private boolean showXSLTResult = false;
    private boolean noDiffsButEquals = false;

    /**
     * Creates new.
     */
    public XsltTestHelper() {
        setExpectedXmlDirectoryForTest(this);
    }

    /**
     * Creates new.
     * @param expectedXmlDirectory expected XML directory.
     */
    public XsltTestHelper(String expectedXmlDirectory) {
        this();
        setExpectedXmlDirectory(expectedXmlDirectory);
    }

    /**
     * Creates new.
     * @param clazz test class to use setup expected XML directory.
     */
    public XsltTestHelper(Class<?> clazz) {
        this();
        setExpectedDirectoryForTestClass(clazz);
    }

    /**
     * Checks whether the XSLT in this test converts the request xml file to the
     * expected outcome.
     * @param anXsltFilename XSLT to apply
     * @param anXmlFilename name of file to transform
     * @param anExpectedOutcomeFile name of file containing expected outcome
     * @throws Exception
     */
    public void compareRequestOutputToFile(String anXsltFilename, String anXmlFilename, String anExpectedOutcomeFile) throws Exception {
        compareRequestOutputToFile(anXsltFilename, null, new HashMap<String, String>(), anXmlFilename, anExpectedOutcomeFile);
    }

    /**
     * Checks whether the XSLT in this test converts the request xml file to the
     * expected outcome, which is expected to a valid message again.
     * @param anXsltFilename XSLT to apply
     * @param anXmlFilename name of file to transform
     * @param anExpectedOutcomeFile name of file containing expected outcome
     * @throws Exception
     */
    public void compareRequestYSDMOutputToFile(String anXsltFilename, String anXmlFilename, String anExpectedOutcomeFile) throws Exception {
        compareRequestOutputToFile(anXsltFilename, null, new HashMap<String, String>(), anXmlFilename, anExpectedOutcomeFile);
        validateAgainstSchema(anExpectedOutcomeFile);
    }
    /**
     * Checks whether the XSLT in this test converts the request xml file to the
     * expected outcome.
     * @param anXsltFilename XSLT to apply
     * @param aParameters parameters to XSLT
     * @param anXmlFilename name of file to transform
     * @param anExpectedOutcomeFile name of file containing expected outcome
     * @throws Exception
     */
    public void compareRequestOutputToFile(String anXsltFilename, Map<String,? extends Object> aParameters, String anXmlFilename, String anExpectedOutcomeFile) throws Exception {
        compareRequestOutputToFile(anXsltFilename, aParameters, new HashMap<String, String>(), anXmlFilename, anExpectedOutcomeFile);
    }

    /**
     * Checks whether the XSLT in this test converts the request xml file to the
     * expected outcome.
     * @param anXsltFilename XSLT to apply
     * @param aParameters parameters to XSLT
     * @param anExpectedDifferences xPath -> expected_value 
     * @param anXmlFilename name of file to transform
     * @param anExpectedOutcomeFile name of file containing expected outcome
     * @throws Exception
     */
    public void compareRequestOutputToFile(String anXsltFilename, Map<String,? extends Object> aParameters, Map<String, String> anExpectedDifferences, String anXmlFilename, String anExpectedOutcomeFile) throws Exception {
        File xslt = new File(xsltDirectory + anXsltFilename);
        // if validation of input request fails we probably changed the XSD
        // but did not yet update the test cases -> broken test to fix
        File xml = validateAgainstSchema(anXmlFilename);

        File output = new File(expectedXmlDirectory + anExpectedOutcomeFile);
        compareOutputToFiles(xslt, aParameters, anExpectedDifferences, xml, output);
    }

    /**
     * Checks whether the XSLT in this test converts the response to the expected xml file
     * @param anXsltFilename XSLT to apply
     * @param anXmlFilename name of file to transform
     * @param anExpectedOutcomeFile name of file containing expected outcome
     * @throws Exception
     */
    public void compareResponseOutputToFile(String anXsltFilename, String anXmlFilename, String anExpectedOutcomeFile) throws Exception {
        compareResponseOutputToFile(anXsltFilename, null, new HashMap<String, String>(), anXmlFilename, anExpectedOutcomeFile);
    }

    /**
     * Checks whether the XSLT in this test converts the response to the expected xml file
     * @param anXsltFilename XSLT to apply
     * @param aParameters parameters to XSLT
     * @param anXmlFilename name of file to transform
     * @param anExpectedOutcomeFile name of file containing expected outcome
     * @throws Exception
     */
    public void compareResponseOutputToFile(String anXsltFilename, Map<String,? extends Object> aParameters, String anXmlFilename, String anExpectedOutcomeFile) throws Exception {
        compareResponseOutputToFile(anXsltFilename, aParameters, new HashMap<String, String>(),
                anXmlFilename, anExpectedOutcomeFile);
    }

    /**
     * Checks whether the XSLT in this test converts the response to the expected xml file
     * @param anXsltFilename XSLT to apply
     * @param aParameters parameters to XSLT
     * @param anExpectedDifferences xpath -> expected_value
     * @param anXmlFilename name of file to transform
     * @param anExpectedOutcomeFile name of file containing expected outcome
     * @throws Exception
     */
    public void compareResponseOutputToFile(String anXsltFilename, Map<String,? extends Object> aParameters, Map<String, String> anExpectedDifferences, String anXmlFilename, String anExpectedOutcomeFile) throws Exception {
        File xslt = new File(xsltDirectory + anXsltFilename);
        File output = getXmlFile(anXmlFilename);
        File xml = new File(expectedXmlDirectory + anExpectedOutcomeFile);
        String result = compareOutputToFiles(xslt, aParameters, anExpectedDifferences, xml, output);

        validateAgainstSchema(xsdFile, "Generated XML", result);
    }

    /**
     * Compares the output of applying an XSLT to an XML to the content of the supplied file
     * 
     * @param xslt
     * @param expectedDifferences
     * @param xml
     * @param output
     * @return generated XML
     * @throws Exception
     */
    public String compareOutputToFiles(File xslt, Map<String,? extends Object> aParameters, Map<String, String> expectedDifferences, File xml, File output) throws Exception {
        assertTrue("XSLT does not exist: " + xslt.getAbsolutePath(), xslt.exists());
        assertTrue("XML does not exist: " + xml.getAbsolutePath(), xml.exists());

        InputSource xmlSource = createInputSource(xml);
        Transform myTransform = new Transform(xmlSource, xslt);
        if (aParameters != null) {
            for (String parameter : aParameters.keySet()) {
                Object value = aParameters.get(parameter);
                myTransform.setParameter(parameter, value);
            }
        }
        String result = myTransform.getResultString();
        if (isShowXSLTResult()) {
            System.out.println(result);
        }
        if (isWriteActual()) {
            String actualFile = writeActualContent(xslt, xml, result);
            System.out.println("Wrote: " + actualFile);
        }
        if (!output.exists()) {
            throw new RuntimeException("expected output does not exist: " + output.getAbsolutePath());
        }
        String myExpectedOutputXML = readFile(output);
        if (!myExpectedOutputXML.equals(result)) {
            if (isNoDiffsButEquals()) {
                // sometime string equals tells you more...
            assertEquals(myExpectedOutputXML, result);
        } else {
            // sometimes this diff is nicer
            List<Difference> unexpectedDifferences = getUnexpectedDifferences(expectedDifferences, myExpectedOutputXML, result);

            if (unexpectedDifferences.size() > 0) {
                fail("Wrong output applying: " + xslt.getAbsolutePath()
                            + " to: " + xml.getAbsolutePath() + "\n"
                            + unexpectedDifferences.toString().replaceAll(",","\n"));
                }
            }
        }

        return result;
    }

    /**
     * Writes actual result of transformation to file.
     *
     * @param xslt xslt applied.
     * @param xml input xml.
     * @param result xslt result.
     * @return filename written.
     * @throws IOException if unable to write.
     */
    protected String writeActualContent(File xslt, File xml, String result) throws IOException {
        String actualFile = getActualFilename(xslt, xml);
        FileOutputStream fw = new FileOutputStream(actualFile);
        fw.write(result.getBytes(FILE_ENCODING));
        fw.flush();
        fw.close();
        return actualFile;
    }

    /**
     * Writes actual result of transformation to file.
     *
     * @param xslt xslt applied.
     * @param xml input xml.
     * @return filename to use.
     */
    protected String getActualFilename(File xslt, File xml) {
        return xml.getName().replace(".xml", "") + ".actual.xml";
    }

    /**
     * Compares two Strings containing XML.
     * @param actualXML actual XML.
     * @param expectedXML expected XML.
     * @return list of differences
     * @throws RuntimeException if differences could not be determined.
     */
    public static List<Difference> getDifferences(
                                        String expectedXML,
                                        String actualXML) {
        return getUnexpectedDifferences(Collections.EMPTY_MAP,
                                        expectedXML, actualXML);
    }

    /**
     * Compares two Strings containing XML.
     * @param actualXML actual XML.
     * @param expectedXML expected XML.
     * @param expectedDifferences xpath -> value that should override content
     *          of expected XML string (i.e. we expect the provided value in
     *          this map and not the one in the expected XML string).
     * @return list of differences
     * @throws RuntimeException if differences could not be determined.
     */
    public static List<Difference> getUnexpectedDifferences(
                                        Map<String, String> expectedDifferences,
                                        String expectedXML,
                                        String actualXML) {
        Class<?> tf = XMLUnit.getTransformerFactory().getClass();
        // Diffs needs a XSLT 1.0 processor
        try {
            if (!DIFF_TRANS_FACT_CLASS.equals(tf)) {
                XMLUnit.setTransformerFactory(DIFF_TRANS_FACT_CLASS.getName());
            }
            Diff d = new Diff(expectedXML, actualXML);
            DetailedDiff detailedDiff = new DetailedDiff(d);
            @SuppressWarnings("unchecked")
            List<Difference> differences = detailedDiff.getAllDifferences();

            List<Difference> unexpectedDifferences = new ArrayList<Difference>();
            for (Difference difference : differences) {
                if (!difference.isRecoverable()) {
                    NodeDetail actual = difference.getTestNodeDetail();
                    String location = actual.getXpathLocation();
                    String value = actual.getValue();

                    if (!expectedDifferences.containsKey(location) || !expectedDifferences.get(location).equals(value)) {
                        unexpectedDifferences.add(difference);
                    }
                }
            }
            return unexpectedDifferences;
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (SAXException e) {
            throw new RuntimeException(e);
        } finally {
            if (!DIFF_TRANS_FACT_CLASS.equals(tf)) {
                XMLUnit.setTransformerFactory(tf.getName());
            }
        }
    }

    /**
     * Reads a file in the directory with expected XML files into a string.
     * @param aFile file to read
     * @return file content
     * @throws java.io.IOException
     */
    public String readFile(String aFile) throws IOException {
        return readFile(getXmlFile(aFile));
    }

    private File getXmlFile(String aFilename) {
        return new File(expectedXmlDirectory + aFilename);
    }

    /**
     * Checks XML file to schema configured via setXsdFile() (if set).
     * @param anXmlFilename xml file.
     * @return File pointing to XML file.
     * @throws Exception if a problem occurred.
     */
    public File validateAgainstSchema(String anXmlFilename) throws Exception {
        return validateAgainstSchema(xsdFile, anXmlFilename);
    }

    /**
     * Checks XML file to schema (if available).
     * @param anXsdFilename schema file.
     * @param anXmlFilename xml file.
     * @return File pointing to XML file.
     * @throws Exception if a problem occurred.
     */
    public File validateAgainstSchema(String anXsdFilename, String anXmlFilename) throws Exception {
        File xmlFile = getXmlFile(anXmlFilename);
        if (anXsdFilename != null) {
            validateAgainstSchema(anXsdFilename, xmlFile.toString(), readFile(xmlFile));
        }
        return xmlFile;
    }

    private void validateAgainstSchema(String xsdFilename, String description, String xml) throws Exception {
        Validator schemaValidator = VALIDATORS.get(xsdFilename);
        if (schemaValidator == null) {
            // get validation driver:
            SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");

            // create schema by reading it from an XSD file:
            schemaValidator = factory.newSchema(new StreamSource(xsdFilename)).newValidator();
            VALIDATORS.put(xsdFilename, schemaValidator);
        }
        try {
            // at last perform validation:
            schemaValidator.validate(new StreamSource(new StringReader(xml)));
        } catch (Exception e) {
            fail(description + " does not conform to schema: " + xsdFilename + ". Message: " + e.getMessage());
        }
    }

    private static InputSource createInputSource(File aFile) throws FileNotFoundException {
        String path = aFile.getAbsolutePath();
        String uri = "file:///" + path;
        InputStream stream = new FileInputStream(path);
        InputSource source = new InputSource(stream);
        source.setSystemId(uri);
        return source;
    }

    private static String readFile(File aFile) throws IOException {
        assertTrue("File does not exist: " + aFile.getAbsolutePath(), aFile.exists());
        FileInputStream stream = new FileInputStream(aFile);
        try {
            FileChannel fc = stream.getChannel();
            MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
            return FILE_ENCODING.decode(bb).toString();
        } finally {
            stream.close();
        }
    }

    public String getXsdFile() {
        return xsdFile;
    }

    public void setXsdFile(String xsdFile) {
        if (xsdFile == null || !xsdFile.equals(this.xsdFile)) {
            this.xsdFile = xsdFile;
        }
    }

    public String getXsltDirectory() {
        return xsltDirectory;
    }

    public void setXsltDirectory(String xsltDirectory) {
        this.xsltDirectory = xsltDirectory;
    }

    public String getExpectedXmlDirectory() {
        return expectedXmlDirectory;
    }

    public void setExpectedXmlDirectory(String expectedXmlDirectory) {
        this.expectedXmlDirectory = expectedXmlDirectory;
    }

    /**
     * Sets directory for expected outcomes at same level as test class' package.
     * @param testClassInstance test class testing XSLTs.
     */
    public void setExpectedXmlDirectoryForTest(Object testClassInstance) {
        Class<?> clazz = testClassInstance.getClass();
        setExpectedDirectoryForTestClass(clazz);
    }

    /**
     * Sets directory for expected outcomes at same level as test class' package.
     * @param clazz class testing XSLTs.
     */
    public void setExpectedDirectoryForTestClass(Class<?> clazz) {
        String pathMatchingClassPackage = getPathMatchingClassPackage(clazz);
        setExpectedXmlDirectory("src/test/resources/" + pathMatchingClassPackage);
    }

    /**
     * @param clazz class to get package from
     * @return directory (in test/resources) matching directory for clazz' package.
     */
    private static String getPathMatchingClassPackage(Class<?> clazz) {
        return clazz.getPackage().getName().replace('.', '/') + "/";
    }

    public boolean isShowXSLTResult() {
        return showXSLTResult;
    }

    public void setShowXSLTResult(boolean showXSLTResult) {
        this.showXSLTResult = showXSLTResult;
    }

    public boolean isNoDiffsButEquals() {
        return noDiffsButEquals;
    }

    public void setNoDiffsButEquals(boolean noDiffsButEquals) {
        this.noDiffsButEquals = noDiffsButEquals;
    }

    public boolean isWriteActual() {
        return writeActual;
    }

    public void setWriteActual(boolean writeActual) {
        this.writeActual = writeActual;
    }
}

