package nl.hsac.test;

import com.ibm.wsdl.xml.WSDLReaderImpl;
import org.springframework.core.io.Resource;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.EndpointInvocationChain;
import org.springframework.ws.server.EndpointMapping;
import org.springframework.ws.server.MessageDispatcher;
import org.springframework.ws.soap.SoapMessage;
import org.springframework.ws.soap.server.endpoint.mapping.SoapActionAnnotationMethodEndpointMapping;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import javax.wsdl.Binding;
import javax.wsdl.BindingOperation;
import javax.wsdl.Definition;
import javax.wsdl.Message;
import javax.wsdl.Operation;
import javax.wsdl.Port;
import javax.wsdl.Service;
import javax.wsdl.extensions.soap.SOAPOperation;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.dom.DOMSource;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Helper to validate an endpoint in spring web services implements all operations in a WSDL.
 */
public class WsdlImplTestHelper {
    /**
     * Tests whether all soap actions in wsdl are mapped to handlers, if SOAP actions are present in request.
     * @param messageDispatcher dispatcher containing endpoint mappings (from Spring configuration)
     * @param wsdl resource containing WSDL definition.
     * @throws Exception if problems are found.
     */
    public void testWsdlImplemented(MessageDispatcher messageDispatcher, Resource wsdl) throws Exception {
        List<EndpointMapping> endpointMappings = messageDispatcher.getEndpointMappings();

        assertNotNull("Null endpoint mappings found in configuration", endpointMappings);
        assertFalse("No endpoint mappings found in configuration", endpointMappings.isEmpty());

        EndpointMapping mapping = endpointMappings.get(0);
        assertTrue("SoapAction based endpoint mapping should be the first (it's fastest), found: " + mapping,
                mapping instanceof SoapActionAnnotationMethodEndpointMapping);

        Definition d = getDefinition(wsdl);
        List<String> soapActions = getSOAPActions(d);
        assertFalse("No SOAP actions found in wsdl definition", soapActions.isEmpty());

        for (String action : soapActions) {
            checkMappingForAction(mapping, action);
        }
    }



    /**
     * Tests whether all request elements in wsdl are mapped to handlers, if SOAP actions are not present so
     * mapping must be done on operation input XML type.
     * @param messageDispatcher dispatcher containing endpoint mappings (from Spring configuration)
     * @param wsdl resource containing WSDL definition.
     * @throws Exception if problems are found.
     */
    public void testWsdlImplementedNoAction(MessageDispatcher messageDispatcher, Resource wsdl) throws Exception {
        List<EndpointMapping> endpointMappings = messageDispatcher.getEndpointMappings();

        assertNotNull("No endpoint mappings found in configuration", endpointMappings);

        Definition d = getDefinition(wsdl);
        List<QName> elements = getRootElements(d);
        assertFalse("No elements found in wsdl definition", elements.isEmpty());

        // we search for elements that are not matched in ANY mapping, different technologies
        // can be used to provide for different elements. That's OK, as long as there are no elements
        // in the WSDL that are not provided at all.
        for (EndpointMapping mapping : endpointMappings) {
            if (!(mapping instanceof SoapActionAnnotationMethodEndpointMapping)) {
                List<QName> nonMatchedElements = new ArrayList<QName>();
                for (QName element : elements) {
                    if (!checkMappingForQName(mapping, element)) {
                        nonMatchedElements.add(element);
                    }
                }
                elements.clear();
                elements.addAll(nonMatchedElements);
            }
        }
        assertTrue("No mappings found for: " + elements, elements.isEmpty());
    }

    private void checkMappingForAction(EndpointMapping soapMapping, String action) throws Exception {
        MessageContext messageContext = mock(MessageContext.class);
        SoapMessage soapRequest = mock(SoapMessage.class);
        when(messageContext.getRequest()).thenReturn(soapRequest);
        when(soapRequest.getSoapAction()).thenReturn(action);

        EndpointInvocationChain endpoint = soapMapping.getEndpoint(messageContext);
        assertNotNull("No mapping for: " + action, endpoint);
    }

    private Definition getDefinition(Resource wsdl) throws Exception {
        InputStream sourceStream = null;
        try {
            sourceStream = wsdl.getInputStream();
            InputSource source = new InputSource(sourceStream);
            WSDLReaderImpl reader = new WSDLReaderImpl();
            return reader.readWSDL(wsdl.getURL().toString(), source);
        } finally {
            if (sourceStream != null) {
                sourceStream.close();
            }
        }
    }

    private boolean checkMappingForQName(EndpointMapping mapping, QName element) throws Exception {
        MessageContext messageContext = mock(MessageContext.class);
        SoapMessage soapRequest = mock(SoapMessage.class);
        when(messageContext.getRequest()).thenReturn(soapRequest);

        Document newDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        Node node = newDocument.createElementNS(element.getNamespaceURI(), element.getLocalPart());
        DOMSource source = new DOMSource(node);
        when(soapRequest.getPayloadSource()).thenReturn(source);

        EndpointInvocationChain endpoint = mapping.getEndpoint(messageContext);
        return endpoint != null;
    }

    @SuppressWarnings("rawtypes")
    private List<String> getSOAPActions(Definition wsdlDefinition) {
        List<String> soapActions = new ArrayList<String>();
        List<BindingOperation> operations = getBindingOperations(wsdlDefinition);
        for (BindingOperation operation : operations) {
            boolean operationHasSOAPAction = false;
            List extElements = operation.getExtensibilityElements();
            for (Object extElem : extElements) {
                if (extElem instanceof SOAPOperation) {
                    SOAPOperation soapOImpl = (SOAPOperation) extElem;
                    String action = soapOImpl.getSoapActionURI();
                    if (action != null && !action.equals("")) {
                        soapActions.add(action);
                        operationHasSOAPAction = true;
                    }
                }
            }
            assertTrue("Expected all operations to have a SOAP action, did not find one on: "
                    + operation, operationHasSOAPAction);
        }
        return soapActions;
    }

    private List<QName> getRootElements(Definition wsdlDefinition) {
        List<QName> qnames = new ArrayList<QName>();
        List<BindingOperation> operations = getBindingOperations(wsdlDefinition);
        for (BindingOperation operation : operations) {
            boolean operationHasElement = false;
            Operation oper = operation.getOperation();
            Message inputMessage = oper.getInput().getMessage();
            if (inputMessage != null) {
                QName inputElement = inputMessage.getQName();
                qnames.add(inputElement);
                operationHasElement = true;
            }
            assertTrue("Expected all operations to have an input message, did not find one on: "
                    + oper.getName(), operationHasElement);
        }
        return qnames;
    }

    @SuppressWarnings("unchecked")
    private List<BindingOperation> getBindingOperations(Definition wsdlDefinition) {
        List<BindingOperation> result = new ArrayList<BindingOperation>();
        Collection<Service> services = wsdlDefinition.getServices().values();
        for (Service service : services) {
            Collection<Port> ports  = service.getPorts().values();
            for (Port port : ports) {
                Binding binding = port.getBinding();
                List<BindingOperation> operations = binding.getBindingOperations();
                result.addAll(operations);
            }
        }
        return result;
    }

}
