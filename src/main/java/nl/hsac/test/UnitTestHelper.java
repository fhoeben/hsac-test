package nl.hsac.test;


import org.springframework.beans.BeanWrapper;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.XMLGregorianCalendar;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.*;
import java.util.Map.Entry;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 *  Helper to use in unit tests.
 */
public final class UnitTestHelper {
    private UnitTestHelper() {
    }

    /**
     * Reads content of UTF-8 file on classpath to String.
     * @param filename file to read.
     * @return file's content.
     */
    public static String loadFile(String filename) {
        String result = null;
        InputStream is = UnitTestHelper.class.getClassLoader().getResourceAsStream(filename);
        if (is == null) {
            throw new IllegalArgumentException("Unable to locate: " + filename);
        }
        try {
            try {
                result = new java.util.Scanner(is, "UTF-8").useDelimiter("\\A").next();
            } catch (java.util.NoSuchElementException e) {
                fail("Unable to read: " + filename + ". Error: " + e.getMessage());
            }
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } catch (IOException e) {
                // what the hell?!
            }
        }
        return result;
    }

    /**
     * Checks whether all expected sub strings are present in actualString.
     * @param actualString string the expectedSubStrings are expected to appear in.
     * @param expectedSubStrings sub strings expected to be present.
     */
    public static void assertContains(String actualString, Object... expectedSubStrings) {
        for (Object expectedSubString : expectedSubStrings) {
            String subString = expectedSubString.toString();
            if (!actualString.contains(subString)) {
                fail(subString + " not found in: " + actualString);
            }
        }
    }

    /**
     * Checks whether all expected elements are present, and no more.
     * @param expectedElements expected elements.
     * @param actualElements actual elements.
     * @param <T> element type
     */
    public static <T> void assertEqualElements(Collection<T> expectedElements, Collection<T> actualElements) {
        for (T element : expectedElements) {
            assertTrue("Expected not found: " + element
                            + ". Actual elements: " + actualElements,
                        actualElements.contains(element));
        }
        for (T element : actualElements) {
            assertTrue("Actual not expected: " + element
                            + ". Actual elements: " + actualElements,
                        expectedElements.contains(element));
        }
    }

    /**
     * Checks whether all expected elements are present, and no more.
     * @param actualElements actual elements.
     * @param expectedElements expected elements.
     * @param <T> element type
     */
    public static <T> void assertEqualElements(Collection<T> actualElements, T... expectedElements) {
        assertEqualElements(Arrays.asList(expectedElements), actualElements);
    }

    /**
     * Checks whether expected and actual are equal, and if not shows which
     * properties differ.
     * @param expected expected object.
     * @param actual actual object
     * @param <T> object type.
     */
    public static <T> void assertEqualsWithDiff(T expected, T actual) {
        Map<String, String[]> diffs = getDiffs(null, expected, actual);

        if (!diffs.isEmpty()) {
            StringBuilder diffString = new StringBuilder();
            for (Entry<String, String[]> diff : diffs.entrySet()) {
                appendDiff(diffString, diff);
            }
            fail(diffs.size() + " difference(s) between expected and actual:\n" + diffString);
        }
    }

    private static void appendDiff(StringBuilder diffString, Entry<String, String[]> diff) {
        String propertyName = diff.getKey();
        String[] value = diff.getValue();
        String expectedValue = value[0];
        String actualValue = value[1];

        diffString.append(propertyName);
        diffString.append(": '");
        diffString.append(expectedValue);
        diffString.append("' <> '");
        diffString.append(actualValue);
        diffString.append("'\n");
    }

    private static Map<String, String[]> getDiffs(String path, Object expected, Object actual) {
        Map<String, String[]> diffs = Collections.emptyMap();
        if (expected == null) {
            if (actual != null) {
                diffs = createDiff(path, expected, actual);
            }
        } else if (!expected.equals(actual)) {
            if (actual == null
                    || isInstanceOfSimpleClass(expected)) {
                diffs = createDiff(path, expected, actual);
            } else if (expected instanceof List) {
                diffs = listDiffs(path, (List) expected, (List) actual);
            } else {
                diffs = getNestedDiffs(path, expected, actual);
            }
            if (diffs.isEmpty() && !(expected instanceof JAXBElement)) {
                throw new IllegalArgumentException("Found elements that are not equal, "
                        + "but not able to determine difference, "
                        + path);
            }
        }
        return diffs;
    }

    private static boolean isInstanceOfSimpleClass(Object expected) {
        return expected instanceof Enum
                || expected instanceof String
                || expected instanceof XMLGregorianCalendar
                || expected instanceof Number
                || expected instanceof Boolean;
    }

    private static Map<String, String[]> listDiffs(String path, List expectedList, List actualList) {
        Map<String, String[]> diffs = new LinkedHashMap<String, String[]>();
        String pathFormat = path + "[%s]";
        for (int i = 0; i < expectedList.size(); i++) {
            String nestedPath = String.format(pathFormat, i);
            Object expected = expectedList.get(i);
            Map<String, String[]> elementDiffs;
            if (actualList.size() > i) {
                Object actual = actualList.get(i);
                elementDiffs = getDiffs(nestedPath, expected, actual);
            } else {
                elementDiffs = createDiff(nestedPath, expected, "<no element>");
            }
            diffs.putAll(elementDiffs);
        }
        for (int i = expectedList.size(); i < actualList.size(); i++) {
            String nestedPath = String.format(pathFormat, i);
            diffs.put(nestedPath, createDiff("<no element>", actualList.get(i)));
        }
        return diffs;
    }

    private static Map<String, String[]> getNestedDiffs(String path, Object expected, Object actual) {
        Map<String, String[]> diffs = new LinkedHashMap<String, String[]>(0);
        BeanWrapper expectedWrapper = getWrapper(expected);
        BeanWrapper actualWrapper = getWrapper(actual);
        PropertyDescriptor[] descriptors = expectedWrapper.getPropertyDescriptors();
        for (PropertyDescriptor propertyDescriptor : descriptors) {
            String propertyName = propertyDescriptor.getName();
            Map<String, String[]> nestedDiffs =
                    getNestedDiffs(path, propertyName,
                            expectedWrapper, actualWrapper);
            diffs.putAll(nestedDiffs);
        }
        return diffs;
    }

    private static Map<String, String[]> getNestedDiffs(
            String path,
            String propertyName,
            BeanWrapper expectedWrapper,
            BeanWrapper actualWrapper) {
        String nestedPath = propertyName;
        if (path != null) {
            nestedPath = path + "." + propertyName;
        }
        Object expectedValue = getValue(expectedWrapper, propertyName);
        Object actualValue = getValue(actualWrapper, propertyName);
        return getDiffs(nestedPath, expectedValue, actualValue);
    }

    private static Map<String, String[]> createDiff(String path, Object expected, Object actual) {
        return Collections.singletonMap(path, createDiff(expected, actual));
    }

    private static String[] createDiff(Object expected, Object actual) {
        return new String[] {getString(expected), getString(actual)};
    }

    private static String getString(Object value) {
        return String.valueOf(value);
    }

    private static Object getValue(BeanWrapper wrapper, String propertyName) {
        Object result = null;
        if (wrapper.isReadableProperty(propertyName)) {
            result = wrapper.getPropertyValue(propertyName);
        } else {
            PropertyDescriptor propertyDescriptor = wrapper.getPropertyDescriptor(propertyName);
            Class<?> propertyType = propertyDescriptor.getPropertyType();
            if (Boolean.class.equals(propertyType)) {
                String name = StringUtils.capitalize(propertyName);
                Object expected = wrapper.getWrappedInstance();
                Method m = ReflectionUtils.findMethod(expected.getClass(), "is" + name);
                if (m != null && m.getReturnType().equals(Boolean.class)) {
                    result = ReflectionUtils.invokeMethod(m, expected);
                } else {
                    throw new IllegalArgumentException(createErrorMsg(wrapper, propertyName));
                }
            } else {
                throw new IllegalArgumentException(createErrorMsg(wrapper, propertyName));
            }
        }
        return result;
    }

    private static String createErrorMsg(BeanWrapper wrapper, String propertyName) {
        return propertyName + " can not be read on: " + wrapper.getWrappedClass();
    }

    private static <T> BeanWrapper getWrapper(T instance) {
        BeanWrapper wrapper = PropertyAccessorFactory.forBeanPropertyAccess(instance);
        wrapper.setAutoGrowNestedPaths(true);
        return wrapper;
    }
}
