package nl.hsac.test;

import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Helper to test XSLTs in a directory, by using inputs and expected values from other directory.
 */
public class DirectoryBasedXsltTestHelper {
    private final PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
    private final MyXsltHelper helper = new MyXsltHelper();
    private String xsltDir;
    private String testsDir;
    private boolean createExpectedIfMissing;

    /**
     * Checks an XSLT using all available inputs.
     * @param xslt stylesheet to test.
     * @return true if all expectations where met.
     * @throws IOException when there were issues reading files.
     */
    public boolean isXsltOk(Resource xslt) throws IOException {
        boolean xsltOk = true;
        Resource[] inputs = findInputs(xslt);
        for (Resource input : inputs) {
            if (!isXsltOkForInput(xslt, input)) {
                xsltOk = false;
            }
        }
        return xsltOk;
    }

    /**
     * Checks an XSLT using one input file.
     * @param xslt stylesheet to test.
     * @param input input file to use.
     * @return true if all expectations where met.
     */
    public boolean isXsltOkForInput(Resource xslt, Resource input) {
        boolean testOk = true;
        try {
            runXsltTest(xslt, input);
        } catch (Throwable t) {
            testOk = false;
        }
        return testOk;
    }

    /**
     * Lists XSLTs to test.
     * @return xslts.
     * @throws java.io.IOException if unable to find.
     */
    public Resource[] findXslts() throws IOException {
        List<Resource> xslFiles = Arrays.asList(resolver.getResources(xsltDir + "*.xsl"));
        List<Resource> xsltFiles = Arrays.asList(resolver.getResources(xsltDir + "*.xslt"));
        List<Resource> resultList = new ArrayList<Resource>(xslFiles.size() + xsltFiles.size());
        resultList.addAll(xslFiles);
        resultList.addAll(xsltFiles);
        return resultList.toArray(new Resource[0]);
    }

    /**
     * Lists inputs to test XSLT with.
     * @return input xml documents.
     * @throws java.io.IOException if unable to find.
     */
    public Resource[] findInputs(Resource xslt) throws IOException {
        String testDir = getTestDir(xslt.getFilename());
        return resolver.getResources(testDir + "*.input.xml");
    }

    /**
     * Determines directory to look for input xml files.
     * @param filename xslt to be tested.
     * @return directory to look for inputs.
     */
    protected String getTestDir(String filename) {
        return testsDir + filename.replace(".xslt", "").replace(".xsl", "") + "/";
    }

    /**
     * Tests an XSLT using an input file.
     * @param xslt stylesheet to test.
     * @param input input to test xslt with.
     * @throws Exception when there was a problem running the test.
     */
    public void runXsltTest(Resource xslt, Resource input) throws Exception {
        String inputFilename = input.getFilename();
        String testName = inputFilename.replace(".input.xml", "");
        Resource expected = input.createRelative(testName + ".expected.xml");
        boolean expectedExists = expected.exists();
        Resource params = input.createRelative(testName + ".input.params");
        Resource actual = input.createRelative(testName + ".actual.xml");
        helper.setActual(actual);

        Resource inputXsd = input.createRelative("input.xsd");
        validateXsd(inputXsd, input);

        Map<String, String> parameters = readParameters(params);
        Map<String, String> expectedDifferences = Collections.emptyMap();
        try {
            helper.compareOutputToFiles(xslt.getFile(),
                    parameters, expectedDifferences,
                    input.getFile(), expected.getFile());

            Resource outputXsd = input.createRelative("output.xsd");
            validateXsd(outputXsd, expected);
        } catch (RuntimeException e) {
            if (createExpectedIfMissing && !expectedExists) {
                FileSystemUtils.copyRecursively(actual.getFile(), expected.getFile());
            }
            throw e;
        }
    }

    private void validateXsd(Resource xsd, Resource xml) {
        if (xsd.exists()) {
            try {
                helper.validateAgainstSchema(getAbsolutePath(xsd), getAbsolutePath(xml));
            } catch (Exception e) {
                throw new RuntimeException("Unable to validate "
                            + xml.getFilename()
                            + " using " + xsd.getFilename(), e);
            }
        }
    }

    private String getAbsolutePath(Resource file) throws IOException {
        return file.getFile().getAbsolutePath();
    }

    private Map<String, String> readParameters(Resource params) throws IOException {
        Map<String, String> result = new HashMap<String, String>();
        if (params != null && params.exists()) {
            Properties p = new Properties();
            p.load(params.getInputStream());
            for (String key : p.stringPropertyNames()) {
                result.put(key, p.getProperty(key));
            }
        }
        return result;
    }

    /**
     * Sets both xslt and test directories, as subdirs of supplied dir.
     * @param aBaseDir directory below which to find xslts and tests (in /xslt/ and /tests/).
     */
    public void setBaseDir(String aBaseDir) {
        setXsltDir(aBaseDir + "xslt/");
        setTestDir(aBaseDir + "tests/");
    }

    /**
     * @param anXsltDir directory to find XSLTs in.
     */
    public void setXsltDir(String anXsltDir) {
        xsltDir = "file:" + anXsltDir;
    }

    /**
     * @param aTestDir directory to find tests in (one subdirectory per XSL).
     */
    public void setTestDir(String aTestDir) {
        testsDir = "file:" + aTestDir;
    }

    /**
     * Sets whether the actual XSLT output should be copied to a (new) expected value, if no expected value
     * was present (the test will still throw an error to inform you no expected value was present).
     * @param aCreateExpectedIfMissing true if actual should be copied.
     */
    public void setCreateExpectedIfMissing(boolean aCreateExpectedIfMissing) {
        createExpectedIfMissing = aCreateExpectedIfMissing;
    }

    private static class MyXsltHelper extends XsltTestHelper {
        private String actualName;

        public MyXsltHelper() {
            setWriteActual(true);
            setExpectedXmlDirectory("");
        }

        @Override
        protected String getActualFilename(File xslt, File xml) {
            return actualName;
        }

        public void setActual(Resource actual) throws IOException {
            actualName = actual.getFile().getAbsolutePath();
        }
    }
}
