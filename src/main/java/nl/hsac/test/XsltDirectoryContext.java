package nl.hsac.test;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
/**
 * Annotation to configure XsltDirectoryRunner.
 */
public @interface XsltDirectoryContext {
    /** Directory containing XSLTs to test. */
    public String xsltDirectory() default "";
    /** Directory containing input to test XSLTs with. */
    public String testDirectory() default "";
    /** Directory with children xslt/, containing XSLTs, and tests/, containing input. */
    public String baseDirectory() default "";
    /** Whether an expected value should be created if not already present. */
    public boolean createExpectedIfMissing() default false;
    /** Whether an expected value should be created if not already present.
     * @deprecated use #createExpectedIfMissing
     */
    public boolean createdExpectedIfMissing() default false;
}
