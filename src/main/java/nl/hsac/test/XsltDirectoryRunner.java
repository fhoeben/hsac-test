package nl.hsac.test;

import org.junit.runner.Description;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.ParentRunner;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * JUnit test runner (use with @RunWith) to test set of XSLTs. Use XsltDirectoryContext to configure it.
 */
public class XsltDirectoryRunner extends ParentRunner<Resource> {
    private DirectoryBasedXsltTestHelper helper;

    /**
     * Constructs a new {@code ParentRunner} that will run {@code @TestClass}
     */
    public XsltDirectoryRunner(Class<?> testClass) throws InitializationError {
        super(testClass);
        helper = new DirectoryBasedXsltTestHelper();
        configureHelper(testClass, helper);
    }

    /**
     * Configures DirectoryBasedXsltTestHelper this class will use.
     * @param aTestClass Java class annotated with this runner.
     * @param aHelper 'fresh' helper to be initialized.
     * @throws InitializationError if helper could not be configured.
     */
    protected void configureHelper(Class<?> aTestClass, DirectoryBasedXsltTestHelper aHelper) throws InitializationError {
        XsltDirectoryContext context = aTestClass.getAnnotation(XsltDirectoryContext.class);
        if (context == null) {
            throw new InitializationError("Please annotate with: " + XsltDirectoryContext.class.getName());
        }
        String baseDir = context.baseDirectory();
        if (!"".equals(baseDir)) {
            aHelper.setBaseDir(baseDir);
        }
        String testDir = context.testDirectory();
        if (!"".equals(testDir)) {
            aHelper.setTestDir(testDir);
        }
        String xsltDir = context.xsltDirectory();
        if (!"".equals(xsltDir)) {
            aHelper.setXsltDir(xsltDir);
        }
        if (context.createExpectedIfMissing()
                || context.createdExpectedIfMissing()) {
            aHelper.setCreateExpectedIfMissing(true);
        }
    }

    @Override
    protected List<Resource> getChildren() {
        try {
            return Arrays.asList(helper.findXslts());
        } catch (IOException e) {
            throw new RuntimeException("Unable to find XSLTs");
        }
    }

    @Override
    protected Description describeChild(Resource xslt) {
        String suiteName = getSuiteName(xslt);
        Description suiteDescription = Description.createSuiteDescription(suiteName);

        for (Resource input : findInputs(xslt)) {
            Description testDescription = getDescription(suiteName, input);
            suiteDescription.addChild(testDescription);
        }
        return suiteDescription;
    }

    private String getSuiteName(Resource xslt) {
        return xslt.getFilename().replace(".xsl", "_xsl");
    }

    private Description getDescription(String suiteName, Resource input) {
        String testName = input.getFilename().replace(".input.xml", "");
        return Description.createTestDescription(suiteName, testName);
    }

    @Override
    protected void runChild(final Resource xslt, RunNotifier notifier) {
        String suiteName = getSuiteName(xslt);
        Resource[] inputs = findInputs(xslt);
        if (inputs.length > 0) {
            for (final Resource input : inputs) {
                Description testDescr = getDescription(suiteName, input);
                Statement statement = new Statement() {
                    @Override
                    public void evaluate() throws Throwable {
                        helper.runXsltTest(xslt, input);
                    }
                };
                runLeaf(statement, testDescr, notifier);
            }
        } else {
            Description suiteDescription = Description.createSuiteDescription(suiteName);
            notifier.fireTestIgnored(suiteDescription);
        }
    }

    private Resource[] findInputs(Resource xslt) {
        try {
            return helper.findInputs(xslt);
        } catch (IOException e) {
            throw new RuntimeException("Error finding inputs");
        }
    }
}
