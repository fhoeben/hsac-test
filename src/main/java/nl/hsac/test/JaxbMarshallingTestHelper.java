package nl.hsac.test;

import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import javax.xml.bind.Marshaller;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;

/**
 *  Helper for unit tests requiring a Jaxb2 marshaller. Since creation of the marshaller can be expensive, it is
 *  cached.
 */
public class JaxbMarshallingTestHelper {
    private static final List<String> CONTEXT_PATHS = new ArrayList<String>();
    private static final Jaxb2Marshaller MARSHALLER = new Jaxb2Marshaller();

    /**
     * Adds supplied context paths to MARSHALLER, all packages should be added before first
     * usage of marshall/unmarshall to ensure they are picked up.
     * @param newContextPaths names of packages with JaxbClasses.
     */
    public static void addContextPaths(String... newContextPaths) {
        boolean added = false;
        for (String path : newContextPaths) {
            if (!CONTEXT_PATHS.contains(path)) {
                CONTEXT_PATHS.add(path);
                added = true;
            }
        }
        if (added) {
            MARSHALLER.setContextPaths(CONTEXT_PATHS.toArray(new String[CONTEXT_PATHS.size()]));
        }
    }

    private final String baseDir;

    /**
     * Creates new.
     * @param aBaseDir directory (in classpath) to find files containing serialized objects.
     */
    public JaxbMarshallingTestHelper(String aBaseDir) {
        if ("".equals(aBaseDir) || aBaseDir.endsWith("/")) {
            baseDir = aBaseDir;
        } else {
            baseDir = aBaseDir + "/";
        }
    }

    /**
     * Unmarshals a file to JAXB object.
     * @param resource file, relative to basedir, to load (from classpath)
     * @return unmarshal'ed object
     */
    public Object unmarshal(String resource) {
        return MARSHALLER.unmarshal(getSource(resource));
    }

    private StreamSource getSource(String resource) {
        return new StreamSource(getClass().getClassLoader().getResourceAsStream(baseDir + resource));
    }

    /**
     * Marshals a JAXB object to a file.
     * Intended more to create expected outcomes once, when defining tests,
     * than to actually use in tests.
     * @param resource file to write to (does not consider basedir).
     * @param jaxbObject object to marshal.
     * @throws RuntimeException if file could not be written.
     */
    public void marshalToFile(String resource, Object jaxbObject) {
        StringWriter outWriter = new StringWriter();
        StreamResult result = new StreamResult(outWriter);
        Map<String, ?> properties = Collections.singletonMap(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        MARSHALLER.setMarshallerProperties(properties);
        MARSHALLER.marshal(jaxbObject, result);

        createFile(resource, outWriter.toString());
    }

    private void createFile(String resource, String xmlString) {
        try {
            PrintWriter writer = new PrintWriter(resource);
            try {
                writer.append(xmlString);
                writer.println();
            } finally {
                writer.flush();
                writer.close();
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
