package nl.hsac.test;

import org.junit.runner.JUnitCore;
import org.junit.runner.RunWith;
import org.junit.runners.model.InitializationError;

@RunWith(CommandLineXsltTestRunner.class)
public class CommandLineXsltTestRunner extends XsltDirectoryRunner {

    private static final String BASE_DIR = "baseDir";
    private static final String TEST_DIR = "testDir";
    private static final String XSLT_DIR = "xsltDir";
    private static final String CREATE_EXPECTED = "createExpected";

    public CommandLineXsltTestRunner(Class<?> testClass) throws InitializationError {
        super(testClass);
    }

    @Override
    protected void configureHelper(Class<?> aTestClass, DirectoryBasedXsltTestHelper aHelper) throws InitializationError {
        String baseDir = getProperty(BASE_DIR);
        if (isFilled(baseDir)) {
            aHelper.setBaseDir(baseDir);
        }
        String testDir = getProperty(TEST_DIR);
        if (isFilled(testDir)) {
            aHelper.setTestDir(testDir);
        }
        String xsltDir = getProperty(XSLT_DIR);
        if (isFilled(xsltDir)) {
            aHelper.setXsltDir(xsltDir);
        }
        String create = getProperty(CREATE_EXPECTED);
        if (isFilled(create)) {
            aHelper.setCreateExpectedIfMissing(Boolean.parseBoolean(create));
        }
    }

    private static void setProperty(String propertyName, String value) {
        System.setProperty(getPropertyName(propertyName), value);
    }

    private static String getProperty(String propertyName) {
        return System.getProperty(getPropertyName(propertyName), "");
    }

    private static String getPropertyName(String propertyName) {
        return "CommandLineXsltTestRunner." + propertyName;
    }

    private static boolean isPropertySet(String propertyName) {
        return isFilled(getProperty(propertyName));
    }
    private static boolean isFilled(String value) {
        return !"".equals(value);
    }

    public static final void main(String... args) {
        if (!propertiesValid()) {
            if (args.length == 1 && !args[0].startsWith("-")) {
                setProperty(BASE_DIR, args[0]);
            } else if (args.length == 2) {
                setProperty(TEST_DIR, args[0]);
                setProperty(XSLT_DIR, args[1]);
            } else {
                System.out.println("Usage:");
                System.out.println("\t<baseDir> | <testDir> <xsltDir>");
                System.out.println("By setting -D" + getPropertyName(CREATE_EXPECTED) + "=false "
                                    +"generating expected outcome in case none is present can be disabled.");
                System.out.println();
            }
        }
        if (propertiesValid()) {
            if (!isPropertySet(CREATE_EXPECTED)) {
                setProperty(CREATE_EXPECTED, "true");
            }
            JUnitCore.main(CommandLineXsltTestRunner.class.getName());
        } else {
            System.err.println("Please specify either baseDir, or testDir and xsltDir.");
            System.exit(-1);
        }
    }

    private static boolean propertiesValid() {
        boolean result = isPropertySet(BASE_DIR);
        if (!result) {
            boolean testDirSet = isPropertySet(TEST_DIR);
            boolean xsltDirSet = isPropertySet(XSLT_DIR);
            result = testDirSet && xsltDirSet;
        }
        return result;
    }
}
